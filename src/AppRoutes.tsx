import React from 'react';
import { useRoutes } from 'react-router-dom';
import AppPage from './pages/AppPage';
import HomePage from './pages/HomePage';

const AppRoutes = () =>
  useRoutes([
    {
      path: '/',
      element: <AppPage />,
      children: [
        { path: '/', element: <HomePage /> },
      ],
    },
  ]);

export default AppRoutes;
